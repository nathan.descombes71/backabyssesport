const functions = require("firebase-functions");
const express = require("express");
// eslint-disable-next-line no-unused-vars
const cors = require("cors");
const admin = require("firebase-admin");
const nodemailer = require("nodemailer");

const teamsApp = express();

teamsApp.use(cors({origin: true}));
admin.initializeApp();

const authMiddleware = require('./authMiddleware');

teamsApp.get("/", async (req, response) => {
    let myArray;
        const snapshot = await admin.firestore().collection("teams").get();
        const teams = [];
        snapshot.forEach((doc) => {
            const id = doc.id;
            const data = doc.data();
            teams.push({
                id,
                ...data,
            });
            myArray = {
                "message": "Teams récupéré avec succès",
                "data": teams,
            };
        });
        response.status(200).send(JSON.stringify(myArray));
});

teamsApp.post("/create", authMiddleware, async (req, resp) => {
    const teams = req.body;

    await admin.firestore().collection("teams").doc(teams["id"]).set(teams);
    resp.status(201).send("teams créé avec succès!");
});

teamsApp.delete("/:id", authMiddleware, async (req, resp) => {
    await admin.firestore().collection("teams").doc(req.params.id).delete();

    resp.status(200).send();
});

teamsApp.put("/:id", authMiddleware, async (req, resp)=>{
    const body = req.body;

    await admin.firestore().collection("teams").doc(req.params.id).update({
        ...body,
    });

    resp.status(200).send();
});

exports.teams = functions.https.onRequest(teamsApp);

// ------------------------------------ //
//                NEWS                  //
// ------------------------------------ //

const newsApp = express();

newsApp.use(cors({origin: true}));

newsApp.get("/", async (req, response) => {
  let myArray;
      const snapshot = await admin.firestore().collection("news").get();
      const news = [];
      snapshot.forEach((doc) => {
          const id = doc.id;
          const data = doc.data();
          news.push({
              id,
              ...data,
          });
          myArray = {
              "message": "News récupéré avec succès",
              "data": news,
          };
      });
      response.status(200).send(JSON.stringify(myArray));
});

newsApp.post("/create", authMiddleware, async (req, resp) => {
  const news = req.body;

  await admin.firestore()
  .collection("news")
  .doc(news["id"]
  .toString())
  .set(news);

  resp.status(201).send("News créé avec succès!");
});

newsApp.delete("/:id", authMiddleware, async (req, resp) => {
  await admin.firestore().collection("news").doc(req.params.id).delete();

  resp.status(200).send();
});

exports.news = functions.https.onRequest(newsApp);

// ------------------------------------ //
//               SPONSOR                //
// ------------------------------------ //

const sponsorApp = express();

// sponsorApp.use(authMiddleware);
sponsorApp.use(cors({origin: true}));

sponsorApp.get("/", async (req, response) => {
  let myArray;
      const snapshot = await admin.firestore().collection("sponsor").get();
      const sponsor = [];
      snapshot.forEach((doc) => {
          const id = doc.id;
          const data = doc.data();
          sponsor.push({
              id,
              ...data,
          });
          myArray = {
              "message": "Sponsor récupéré avec succès",
              "data": sponsor,
          };
      });
      response.status(200).send(JSON.stringify(myArray));
});

sponsorApp.post("/create", authMiddleware, async (req, resp) => {
  const sponsor = req.body;

  await admin.firestore()
  .collection("sponsor")
  .doc(sponsor["id"]
  .toString())
  .set(sponsor);
  resp.status(201).send("Sponsor créé avec succès!");
});

sponsorApp.delete("/:id", authMiddleware, async (req, resp) => {
  await admin.firestore().collection("sponsor").doc(req.params.id).delete();

  resp.status(200).send();
});

exports.sponsor = functions.https.onRequest(sponsorApp);

// ------------------------------------ //
//              SEND EMAIL              //
// ------------------------------------ //

const sendMail = express();

sendMail.use(cors({origin: true}));

const transport = nodemailer.createTransport({
    host: "smtp.ionos.fr",
    port: 465,
    secure: true,
    auth: {
      user: "flood@abyss-esport.com",
      pass: "AbyssEsport123!!Spam",
    },
  });

sendMail.post("/", async (req, resp) => {
    const maill = req.body;
    const mailOptions = {
        from: maill["mail"], // Adding sender's email
        to: "contact@abyss-esport.com",
        subject: maill["subject"], // Email subject
        html: "<b>Message de " + maill["firstname"] + " '" + maill["pseudo"] + "' " + maill["lastname"] + ": " + maill["message"] + "</b>",
    };
    return transport.sendMail(mailOptions, (err, info) => {
        if (err) {
            return resp.send(err.toString());
        }
        return resp.send("Email sent successfully");
    });
  });
  exports.sending = functions.https.onRequest(sendMail);

// ------------------------------------ //
//               TESTAPI                //
// ------------------------------------ //

exports.helloWorld = functions.https.onRequest((request, response) => {
    // functions.logger.info("Hello logs!", {structuredData: true });
    response.send("Hello from Firebase!");
});
